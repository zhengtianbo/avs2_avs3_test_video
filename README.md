#AVS2/AVS3/CAVS测试视频(AVS3/AVS2/CAVS sample videos)。<br>
下载ffmpeg压播工具 (Download ffmpeg test build for Windows)：https://gitee.com/zhengtianbo/FFmpeg-avs2-avs3/releases

#压缩(Encoding)：

```
ffmpeg -i 篮球测试视频一.mp4 -vcodec avs2 -acodec copy 篮球测试视频一_AVS2.mkv
...
frame=  500 fps= 45 q=-0.0 Lsize=    6517kB time=00:00:19.98 bitrate=2671.8kbits/s speed=1.79x

ffmpeg -i 篮球测试视频一.mp4 -vcodec libx265 -acodec copy 篮球测试视频一_x265.mkv
...
frame=  500 fps=8.9 q=35.6 Lsize=    8244kB time=00:00:19.98 bitrate=3379.8kbits/s speed=0.357x

```

参数speed_level调整画质:

```
ffmpeg -i 篮球测试视频一.mp4 -vcodec avs2 -speed_level 4 -acodec copy 篮球测试视频一_AVS2.mkv
```

speed_level越大画质越高但越慢.

avs3类似, 调速参数为speed：

```
ffmpeg -i xxx_x264.mkv -vcodec avs3 -acodec copy xxx_avs3.mkv
ffmpeg -i xxx_x264.mkv -vcodec avs3 -speed 2 -acodec copy xxx_avs3.mkv
```


#播放(Playing)：

```
ffplay 源视频样本一_AVS2.mkv
ffplay masterchef.australia.s12e59.hdtv.avs3-fqm[eztv.io].mkv
```
或使用支持CAVS/AVS2/AVS3的VLC及mpc-hc播放器 (Or you can use other CAVS/AVS2/AVS3 enabled video players).<br>
下载mpc-hc:https://gitee.com/zhengtianbo/cavs-avs2-avs3_decoder_added_to_mpc_hc/releases<br>
下载VLC 4:https://gitee.com/zhengtianbo/AVS123_VLC4_Player/releases<br>
下载VLC 3:https://gitee.com/zhengtianbo/VLC3-AVS3AVS2CAVS/releases
